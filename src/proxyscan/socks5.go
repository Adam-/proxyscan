package main

import (
	"net"
	"strconv"
)

func socks5(settings scan_settings, port int) {
	targetAddr, _ := net.ResolveTCPAddr("tcp4", settings.destIp + ":" + strconv.Itoa(settings.destPort))

	q1 := targetAddr.IP[len(targetAddr.IP) - 4]
	q2 := targetAddr.IP[len(targetAddr.IP) - 3]
	q3 := targetAddr.IP[len(targetAddr.IP) - 2]
	q4 := targetAddr.IP[len(targetAddr.IP) - 1]

	buf := []byte{
		5, // Version
		1, // # of methods
		0, // No authentication

		5, // Version
		1, // Connect
		0, // Reserved
		1, // IPv4
	}
	buf = append(buf, []byte{
		q1,
		q2,
		q3,
		q4,
	}...)
	buf = append(buf, []byte{
		byte(targetAddr.Port >> 8),
		byte(targetAddr.Port),
	}...)

	scan(settings, port, false, "socks5", buf)
}
