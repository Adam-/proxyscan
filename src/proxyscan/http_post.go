package main

import (
	"fmt"
)

func http_post_scan(settings scan_settings, port int, name string, use_tls bool) {
	buf := fmt.Sprintf("POST http://%s:%d/ HTTP/1.0\r\n" +
						"Content-length: 0\r\n" +
						"Connection: close\r\n" +
						"\r\n",
						settings.destIp, settings.destPort)
	scan(settings, port, use_tls, name, []byte(buf))
}

func http_post(settings scan_settings, port int) {
	http_post_scan(settings, port, "http_post", false)
}

