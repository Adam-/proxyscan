package main

import (
	"fmt"
)

func http_connect_scan(settings scan_settings, port int, name string, use_tls bool) {
	buf := fmt.Sprintf("CONNECT %s:%d HTTP/1.0\r\n\r\n", settings.destIp, settings.destPort)
	scan(settings, port, use_tls, name, []byte(buf))
}

func http_connect(settings scan_settings, port int) {
	http_connect_scan(settings, port, "http_connect", false)
}

