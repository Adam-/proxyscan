package main

import (
	"net"
	"strconv"
)

func socks4(settings scan_settings, port int) {
	targetAddr, _ := net.ResolveTCPAddr("tcp4", settings.destIp + ":" + strconv.Itoa(settings.destPort))

	q1 := targetAddr.IP[len(targetAddr.IP) - 4]
	q2 := targetAddr.IP[len(targetAddr.IP) - 3]
	q3 := targetAddr.IP[len(targetAddr.IP) - 2]
	q4 := targetAddr.IP[len(targetAddr.IP) - 1]

	buf := []byte{
		4, // Version
		1, // Connect
		byte(targetAddr.Port >> 8),
		byte(targetAddr.Port),
		q1,
		q2,
		q3,
		q4,
		0, // No authentication
	}

	scan(settings, port, false, "socks4", buf)
}
