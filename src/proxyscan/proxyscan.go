package main

import (
	"io/ioutil"
	"encoding/json"
	"fmt"
	"flag"
	"net"
	"strconv"
	"strings"
	"sync"
	"os"
	"time"
	"crypto/tls"
)

var config map[string]interface{}
var wg sync.WaitGroup
var debug *bool

type scan_settings struct {
	targetIp *string
	bindIp *string
	destIp string
	destPort int
	destString string
}

type scan_func func(scan_settings, int)

func proxyscan(settings scan_settings, protocol string, ports []interface{}) {
	var f scan_func = nil

	if protocol == "http_connect" {
		f = http_connect
	} else if protocol == "http_post" {
		f = http_post
	} else if protocol == "http_put" {
		f = http_put
	} else if protocol == "socks4" {
		f = socks4
	} else if protocol == "socks5" {
		f = socks5
	} else if protocol == "cisco" {
		f = cisco
	} else if protocol == "https_connect" {
		f = https_connect
	} else if protocol == "https_post" {
		f = https_post
	} else if protocol == "https_put" {
		f = https_put
	} else if protocol == "http_auth" {
		f = http_auth
	}

	if f == nil {
		fmt.Printf("Unknown protocol type %s\n", protocol)
		return
	}

	for _, port := range ports {
		p := int(port.(float64))
		wg.Add(1)
		go func() {
			defer wg.Done()
			f(settings, p)
		}()
	}
}

func connect(settings scan_settings, port int, protocol string, use_tls bool) net.Conn {
	bindAddr, _ := net.ResolveTCPAddr("tcp", *settings.bindIp + ":0")

	var con net.Conn
	var err error
	if use_tls {
		dialer := net.Dialer{
			LocalAddr: bindAddr,
		}
		tlsconfig := tls.Config{
			InsecureSkipVerify: true,
		}
		con, err = tls.DialWithDialer(&dialer, "tcp", *settings.targetIp + ":" + strconv.Itoa(port), &tlsconfig)
	} else {
		targetAddr, _ := net.ResolveTCPAddr("tcp", *settings.targetIp + ":" + strconv.Itoa(port))

		con, err = net.DialTCP("tcp", bindAddr, targetAddr)
	}
	if err != nil {
		if *debug {
			fmt.Printf("Unable to connect to %s:%d (%s): %s\n", *settings.targetIp, port, protocol, err)
		}
		return nil
	}
	if *debug {
		fmt.Printf("Connected to %s:%d (%s)\n", *settings.targetIp, port, protocol)
	}
	return con
}

func scan(settings scan_settings, port int, use_tls bool, protocol string, buffer []byte) {
	con := connect(settings, port, protocol, use_tls)

	if con == nil {
		return
	}

	defer con.Close()

	if *debug {
		fmt.Printf("Connected to %s:%d (%s)\n", *settings.targetIp, port, protocol)
	}

	bytes, err := con.Write(buffer)
	if err != nil {
		if *debug {
			fmt.Printf("Unable to write to %s:%d (%s): %s\n", *settings.targetIp, port, protocol, err)
		}
		return
	}
	if *debug {
		fmt.Printf("Wrote %d bytes to %s:%d (%s): %s\n", bytes, *settings.targetIp, port, protocol, string(buffer))
	}

	reply := make([]byte, 1024)
	for {
		bytes, err = con.Read(reply)
		if err != nil {
			if *debug {
				fmt.Printf("Unable to read from %s:%d (%s): %s\n", *settings.targetIp, port, protocol, err)
			}
			return
		}
		if *debug {
			fmt.Printf("Read %d bytes from %s:%d (%s): %s\n", bytes, *settings.targetIp, port, protocol, string(reply))
		}

		con.Write([]byte(protocol + ":" + *settings.targetIp + ":" + strconv.Itoa(port) + "\r\n"))

		if strings.Index(string(reply), settings.destString) != -1 {
			hit(settings, protocol, port)
		}
	}
}

func hit(settings scan_settings, protocol string, port int) {
	fmt.Printf("%s %s:%d open\n", *settings.targetIp, protocol, port)
	os.Exit(1)
}

func main() {
	var settings scan_settings

	settings.targetIp = flag.String("targetip", "", "")
	settings.bindIp = flag.String("bindip", "", "")
	debug = flag.Bool("debug", false, "")

	flag.Parse()

	if settings.targetIp == nil || *settings.targetIp == "" {
		panic("no target ip")
	}

	file, err := ioutil.ReadFile("proxyscan.conf")
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(file, &config)
	if err != nil {
		panic(err)
	}

	settings.destIp = config["dest"].(string)
	settings.destPort = int(config["destPort"].(float64))
	settings.destString = config["destString"].(string)
	timeout := int(config["timeout"].(float64))

	if *debug {
		fmt.Printf("Target %s, Bind %s, Dest %s:%d\n", *settings.targetIp, *settings.bindIp, settings.destIp, settings.destPort)
	}

	scan := config["scan"].(map[string]interface{})
	for proto, ports := range scan {
		proxyscan(settings, proto, ports.([]interface{}))
	}

	go func() {
		time.Sleep(time.Duration(timeout) * time.Second)
		if *debug {
			fmt.Printf("Timed out\n")
		}
		os.Exit(-1)
	}()

	wg.Wait()

	if *debug {
		fmt.Printf("Scan finished, nothing detected\n")
	}
	os.Exit(0)
}

